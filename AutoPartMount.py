#this is a script for automatically detecting, partitioning, and mounting disks. 
#It sets the disks to automount by creating entries in /etc/fstab
import sys,subprocess,re

#Get user input to determine which disks (if any)
#they would like this script to leave untouched

def create_protec_list(x):
    pre_avoidmelist = x.replace(" ",",").split(",")
    avoidmelist = []
    for drive in pre_avoidmelist:
        if drive == '':
            pass
        else:
            avoidmelist += [drive]
    return avoidmelist
    
#check to see what disks exist and if/where they are 
#mounted, return list of relevant mounted and unmounted disks
def check_for_disks():
    cmd = 'lsblk -l -o name,mountpoint'.split()
    driveDict = {}
    finalDict = {}
    diskFile = open("disks.txt", "x")
    subprocess.call(cmd, stdout=diskFile)
    diskFile.close()
    diskFile = open("disks.txt", "r")
    for line in diskFile.readlines():
        dictItem = line.split()
        try:
            driveDict[dictItem[0]] = dictItem[1]
        except(IndexError):
            driveDict[dictItem[0]] = "No MountPoint Defined"
    diskFile.close()
    for k, v in driveDict.items():
        if 'sd' in k:
            finalDict['/dev/'+ k] = v
        elif 'nv' in k:
            finalDict['/dev/'+ k] = v
        else:
            pass
    return finalDict

#this next one is so we can separate all drives collected from the user as a "do not touch" drive
#and all drives collected from the machine

def sort_disks(x,y):
    driveList = []
    for k, v in x.items():
        for drive in y:
            if drive in k:
                break
        else:
            driveList += [k]
    return driveList

#This function cleans up any partitions that were collected in the check_for_disks() function 
#so we don't run into errors trying to operate on a partition

def cleanuplist(x):
    newlist = []
    for drive in x:
        y= re.findall("[0-9]", drive)
        if not y:
            newlist += [drive]
    return newlist

#this function does as it states and uses fdisk to make a gpt table and a single partition on the selected drives 
def makePartitions(x):
    for disk in x:
        p1 = subprocess.Popen(['printf', 'g\nn\n1\n\n\nw\n'], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        p2 = subprocess.Popen(['fdisk', disk], stdin=p1.stdout, stderr=subprocess.PIPE, stdout=subprocess.PIPE).wait()

def make_filesystem(x):
    y = 0
    while y == 0:
        fstype = input("Choose a filesystem for your drives (eg. ext4): ")
        if fstype.lower() == 'ext2':
            for drive in x:
                cmd = 'mkfs.ext2 ' + drive + '1'
                subprocess.call(cmd, shell=True)
            y += 1
        elif fstype.lower() == 'ext3':
            for drive in x:
                cmd = 'mkfs.ext3 ' + drive + '1'
                subprocess.call(cmd, shell=True)
            y += 1
        elif fstype.lower() == 'ext4':
            for drive in x:
                cmd = 'mkfs.ext4 ' + drive + '1'
                subprocess.call(cmd, shell=True)
            y += 1
        elif fstype.lower() == 'bfs':
            for drive in x:
                cmd = 'mkfs.bfs ' + drive + '1'
                subprocess.call(cmd, shell=True)
            y += 1
        elif fstype.lower() == 'btrfs':
            for drive in x:
                cmd = 'mkfs.btrfs ' + drive + '1'
                subprocess.call(cmd, shell=True)
            y += 1
        elif fstype.lower() == 'cramfs':
            for drive in x:
                cmd = 'mkfs.cramfs ' + drive + '1'
                subprocess.call(cmd, shell=True)
            y += 1
        elif fstype.lower() == 'fat':
            for drive in x:
                cmd = 'mkfs.fat ' + drive + '1'
                subprocess.call(cmd, shell=True)
            y += 1
        elif fstype.lower() == 'minix':
            for drive in x:
                cmd = 'mkfs.minix ' + drive + '1'
                subprocess.call(cmd, shell=True)
            y += 1
        elif fstype.lower() == 'msdos':
            for drive in x:
                cmd = 'mkfs.msdos ' + drive + '1'
                subprocess.call(cmd, shell=True)
            y += 1
        elif fstype.lower() == 'ntfs':
            for drive in x:
                cmd = 'mkfs.ntfs ' + drive + '1'
                subprocess.call(cmd, shell=True)
            y += 1
        elif fstype.lower() == 'vfat':
            for drive in x:
                cmd = 'mkfs.vfat ' + drive + '1'
                subprocess.call(cmd, shell=True)
            y += 1
        elif fstype.lower() == 'xfs':
            for drive in x:
                cmd = 'mkfs.xfs ' + drive + '1'
                subprocess.call(cmd, shell=True)
            y += 1
        else:
            print("The filesystem you entered was either entered incorrectly, or is unavailable with this script.")
            print("")
            print("")

def confirm_UUID():
    cmd = 'partprobe'
    subprocess.call(cmd, shell=True)
#This determines the UUID for our newly formatted drives, and creates a dictionary
def get_uuid_for_disk():
    cmd = 'lsblk -l -o name,UUID'.split()
    uuidDict = {}
    almostDict = {}
    finalDict = {}
    uuidFile = open("uuid.txt", "x")
    subprocess.call(cmd, stdout=uuidFile)
    uuidFile.close()
    uuidFile = open("uuid.txt", "r")
    for line in uuidFile.readlines():
        uuidInfo = line.split()
        try:
            uuidDict[uuidInfo[0]] = uuidInfo[1]
        except(IndexError):
            uuidDict[uuidInfo[0]] = "No UUID"
    uuidFile.close()
    for k, v in uuidDict.items():
        if 'sd' in k:
            almostDict['/dev/'+ k] = v
        elif 'nv' in k:
            almostDict['/dev/'+ k] = v
        else:
            pass
    for k, v in almostDict.items():
        if "No UUID" in v:
            pass
        else:
            finalDict[k] = v
    return finalDict


def sort_uuid(x,y):
    uuidDict = {}
    for k, v in x.items():
        for drive in y:
            if drive in k:
                break
        else:
            uuidDict[k] = v
    return uuidDict

def create_mount_point(x):
    cmd = 'mkdir /data'
    subprocess.call(cmd, shell=True)
    for number in range(0,len(x)):
        cmd = 'mkdir /data/' + str(number)
        subprocess.call(cmd, shell=True)

#check /etc/fstab to see if the disk already has an entry
def check_fstab_and_mount(x):
    fs = open("/etc/fstab", "r+")
    counter = 0
    for k,v in x.items():
        for line in fs.readlines():
            if v in line:
                break
        else:
            fs.write('/dev/disk/by-uuid/' + v + ' /data/' + str(counter) + ' ext4    defaults   0   2\n')
            counter += 1
    fs.close()
    cmd = 'mount -a'
    subprocess.call(cmd, shell=True)



def will_not_touch(x):
    print("I will NOT perform any operation on the selected drives: ")
    for drive in x:
        print(drive)
    print("")
    print("")
          
def will_alter(x):
    print("I will, however, be formatting, and mounting the following drives: ")
    for drive in x:
        print(drive)
    print("")
    print("")

def program():
    backup = 'cp /etc/fstab /etc/fstab.bak'
    subprocess.call(backup, shell=True)
    fstab = '/etc/fstab'
    avoidme = input("If you would like this script to skip operation on any disks, enter them here, separated by a single space (e.g /dev/sda /dev/sdc): ")
    avoidmelist = create_protec_list(avoidme)
    diskDict = check_for_disks()
    will_not_touch(avoidmelist)
    driveList = sort_disks(diskDict,avoidmelist)
    sortedDlist = cleanuplist(driveList)
    will_alter(sortedDlist)
    while True:
            s1 = input("would you like to proceed(y/n)?: ")
            if s1.lower() == "y":
                makePartitions(sortedDlist)
                make_filesystem(sortedDlist)
                confirm_UUID()
                uuidDict = get_uuid_for_disk()
                finalUUID = sort_uuid(uuidDict,avoidmelist)
                create_mount_point(finalUUID)
                check_fstab_and_mount(finalUUID)
                cleanup1 = 'rm disks.txt'
                subprocess.call(cleanup1, shell=True)
                cleanup2 = 'rm uuid.txt'
                subprocess.call(cleanup2, shell=True)
                sys.exit(0)
            elif s1.lower() == "n":
                cleanup1 = 'rm disks.txt'
                subprocess.call(cleanup1, shell=True)
                cleanup2 = 'rm uuid.txt'
                subprocess.call(cleanup2, shell=True)
                sys.exit(0)
            else:
                print("You didn't select 'y' or 'n' - going with 'n' on this one!")
                cleanup1 = 'rm disks.txt'
                subprocess.call(cleanup1, shell=True)
                cleanup2 = 'rm uuid.txt'
                subprocess.call(cleanup2, shell=True)
                sys.exit(0)

program()