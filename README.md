# Hard Drive Partition and Mount

I wanted a simple script to manage setting up my hard drives on servers to have 1 partition, and auto mount on startup. 

If you read my code and know of a more elegant way to do what I have done/written, by all means submit it, I want to make this as clean, secure, and efficient as possible.